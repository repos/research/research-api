import functools

from enum import StrEnum, unique

import pydantic_settings as pyds


@unique
class LogLevel(StrEnum):
    CRITICAL = "CRITICAL"
    ERROR = "ERROR"
    WARNING = "WARNING"
    INFO = "INFO"
    DEBUG = "DEBUG"


class Config(pyds.BaseSettings):
    """Configuration for the API.

    These fields can be changed using the constructor,
    environment variables or a dotenv (.env) file, in decreasing
    order of priority. See the pydantic docs for details:
    https://docs.pydantic.dev/latest/concepts/pydantic_settings/#field-value-priority
    """

    api_prefix: str = "/api"
    """Prefix for all api routes e.g. /`api_prefix`/health."""
    log_level: LogLevel = LogLevel.INFO
    """Logging level for both the root and the uvicorn logger."""
    greeting: str = "Hello"
    """Greeting message for the example greeting endpoint."""


@functools.lru_cache
def get_config() -> Config:
    return Config()
