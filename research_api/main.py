import fastapi

from research_api.app import create_app
from research_api.config import Config, get_config

config: Config = get_config()
app: fastapi.FastAPI = create_app(config=config)
