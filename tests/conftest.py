import fastapi
import pytest

from fastapi.testclient import TestClient as FastAPITestClient

from research_api.app import create_app
from research_api.config import Config


@pytest.fixture(scope="session")
def config() -> Config:
    return Config(api_prefix="", greeting="Test hello")


@pytest.fixture(scope="session")
def app(config: Config) -> fastapi.FastAPI:
    return create_app(config=config)


@pytest.fixture(scope="function")
def test_client(app: fastapi.FastAPI) -> FastAPITestClient:
    return FastAPITestClient(app=app)
